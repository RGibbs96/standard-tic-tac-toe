def print_board(entries):
    # creating string line, composed of + and -
    line = "+---+---+---+"
    # setting output to previously created line
    output = line
    n = 0
    # create for loop to iterate over the passed argument
    for entry in entries:
        # n is divisible by 3, add a new line and |
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()


def game_over(board,win_letter):
    # some stuff here that you need to write
    print_board(board)
    print("GAME OVER", win_letter, "has won")
    exit()

def top_row_is_winner(board):
    return board[0] == board[1] and board[1] == board[2]

def middle_row_is_winner(board):
    return board[3] == board[4] and board[4] == board[5]

def bottom_row_is_winner(board):
    return board[6] == board[7] and board[7] == board[8]

def left_column_is_winner(board):
    return board[0] == board[3] and board[3] == board[6]

def middle_column_is_winner(board):
    return board[1] == board[4] and board[4] == board[7]

def right_column_is_winner(board):
    return board[2] == board[5] and board[5] == board[8]

def left_diagonal_is_winner(board):
    return board[0] == board[4] and board[4] == board[8]

def right_diagonal_is_winner(board):
    return board[2] == board[4] and board[4] == board[6]

def is_row_winner(board, row_number):
    # boolean expression for row winner
    return board[0+((row_number-1)*3)] == board[1+((row_number-1)*3)] and board[1+((row_number-1)*3)] == board[2+((row_number-1)*3)]

def is_col_winner(board, col_num):
    # boolean expression for col winner
    return board[0 + (col_num-1)] == board[3 + (col_num-1)] and board[3 + (col_num-1)] == board[6 + (col_num-1)]

def is_diag_winner(board):
    # boolean expression for diag winnder
    return (board[0] == board[4] and board[4] == board[8]) or (board[2] == board[4] and board[4] == board[6])

board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_row_winner(board, 1):
        game_over(board, board[0])
    elif is_row_winner(board, 2):
        game_over(board, board[3])
    elif is_row_winner(board, 3):
        game_over(board,board[6])
    elif is_col_winner(board, 1):
        game_over(board,board[0])
    elif is_col_winner(board, 2):
        game_over(board,board[1])
    elif is_col_winner(board, 3):
        game_over(board,board[2])
    # calling is_diag_winner once for the win condition as there are only two win conditions
    # that will overlap the middle position
    elif is_diag_winner(board):
        game_over(board,board[4])

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
